# WordPress Admin Cleanup Controls Plugin

This WordPress plugin hides various options in the WordPress Admin that you don't want your client to see but allow them to still have the access if they opt-in. Please note this does not limit access and is not a security alternative, it just hides with an option to see menu items.

1. Download / Install as a plugin.
2. Settings are for Admins at Appearence -> Admin Controls


## Notes
- Hides the following pages by default: customize, comments, settings, plugins, tools, themes (apart from menus), jetpack, yoast, advanced custom fields.

- Only for Administrators (and whoever else has the manage_options capability).

- Please do not use this a security plugin, this does not limit access to the hidden pages it's purpose is to simply hide menu items to clean up the Admin area for yourself / clients without you having to hold back access from them.

- If you want to limit access to pages then you may wish to look at something like: https://en-gb.wordpress.org/plugins/adminimize/


## Hire me
Hey! I'm a [freelance WordPress developer and technical consultant](https://mostlycaffeine.com) with a mission to deliver accessible, performant, secure websites that are sustainable and respect human rights. I'm available to hire for full projects or contract work, and would love to work with you!
- https://mostlycaffeine.com
<?php
/**
 * Admin Cleanup Controls Plugin
 *
 *
 * @link              https://mostlycaffeine.com
 * @since             1.0.0
 * @package           admincleanupcontrols
 *
 * @wordpress-plugin
 * Plugin Name:       Admin Cleanup Controls
 * Plugin URI:        https://mostlycaffeine.com/
 * Description:       Hide various options in the WordPress Admin that you don't want your client to see but allow them to still have the access if they opt-in. Please note this does not limit access and is not a security alternative, it just hides with an option to see menu items.
 * Version:           1.0.0
 * Author:            @mostlycaffeine
 * Author URI:        https://mostlycaffeine.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       admincleanupcontrols
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {

	die;

}


/**
 * Current plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 * No ! define because it shouldn't be overwritten by anything else.
 */
define( 'ADMINCLEANUPCONTROLS_VERSION', '1.0.0' );


if ( ! defined( 'ADMINCLEANUPCONTROLS_PLUGIN_DIR' ) ) {

	define( 'ADMINCLEANUPCONTROLS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

}


class MC_AdminCleanupControls {

	public function __construct() {

		// Calling pluggable to get access to user info.
		if ( ! function_exists( 'wp_get_current_user' ) ) {

			include ABSPATH . 'wp-includes/pluggable.php';

		}

		// Only applicable to Admins.
		if ( current_user_can( 'manage_options' ) ) {

			// Add menu and page.
			add_action( 'admin_menu', array( $this, 'mc_admincleanupcontrols_submenu' ), 10 );

			// Check if user has already set their Advanced Controls preference.
			$current_user_id = get_current_user_id();
			$value           = get_user_meta( $current_user_id, '_show_hide_admin' );

			// Clean up the Admin if "All" is not set in the advanced settings.
			if ( 'all' !== $value[0] ) {

				// Main & sub pages to remove.
				add_action( 'admin_menu', array( $this, 'mc_admincleanupcontrols_default' ), 11 );

				// Advanced Custom Fields.
				add_filter( 'acf/settings/show_admin', '__return_false' );

				// Yoast.
				remove_action( 'admin_bar_menu', 'wpseo_admin_bar_menu', 95 );

			}
		}

	}

	/**
	 * Cleanup the menu items.
	 */
	public function mc_admincleanupcontrols_default() {

		// Seemingly the simplest way to hide the customize pages.
		$customize_url        = 'customize.php?return=' . rawurlencode( $_SERVER['REQUEST_URI'] );
		$customize_url_header = $customize_url . '&#038;autofocus%5Bcontrol%5D=header_image';
		$customize_url_bg     = $customize_url . '&#038;autofocus%5Bcontrol%5D=background_image';

		// Main pages to remove from menu.
		$main_menu_pages = array(
			'customize.php',
			'edit-comments.php',
			'options-general.php',
			'plugins.php',
			'tools.php',
			// Plugins
			'jetpack',
			'wpseo_dashboard',
		);

		// Sub pages to remove from menu.
		$sub_menu_pages = array(
			'index.php'  => [ 'update-core.php' ],
			'themes.php' => [ 'themes.php', 'theme-editor.php', 'widgets.php', $customize_url, $customize_url_header, $customize_url_bg ],
		);

		if ( isset( $GLOBALS['menu'] ) && is_array( $GLOBALS['menu'] ) ) {

			// Remove the main pages.
			foreach ( $main_menu_pages as $main_menu_page ) {

				remove_menu_page( $main_menu_page );

			}

			// Remove sub pages.
			foreach ( $sub_menu_pages as $parent_menu_page => $sub_menu_page ) {

				foreach ( $sub_menu_page as $sub_menu_page_remove ) {

					remove_submenu_page( $parent_menu_page, $sub_menu_page_remove );

				}
			}
		}

	}

	/**
	 * Add menu and settings page as Appearence -> Admin Controls.
	 */
	public function mc_admincleanupcontrols_submenu() {

		add_submenu_page(
			'themes.php',
			__( 'Admin Controls', 'admincleanupcontrols' ),
			__( 'Admin Controls', 'admincleanupcontrols' ),
			'manage_options',
			'admin-cleanup-controls',
			array( &$this, 'mc_admincleanupcontrols_settingpage' )
		);

	}

	/**
	 * Settings page content with form.
	 */
	public function mc_admincleanupcontrols_settingpage() {

		// Check if the form has been posted.
		if ( isset( $_REQUEST['_wpnonce'] ) && isset( $_POST['show_hide_admin'] ) ) {

			$nonce           = sanitize_key( $_REQUEST['_wpnonce'] );
			$show_hide_admin = sanitize_text_field( $_POST['show_hide_admin'] );

			// Check if the nonce is correct for security.
			if ( wp_verify_nonce( $nonce, 'mc-show-hide-admin-nonce' ) ) {

				$current_user_id = get_current_user_id();

				// Save users preference.
				update_user_meta( $current_user_id, '_show_hide_admin', $show_hide_admin );

			}
		}

		// Check the users preference and display current prefence in dropdown.
		$current_user_id = get_current_user_id();
		$preference      = get_user_meta( $current_user_id, '_show_hide_admin' );

		if ( 'all' === $preference[0] ) {

			$selected = 'selected';

		} else {

			$selected = '';

		}

		// Page & form output with nonce.
		echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
		echo '<h2>';
		esc_html_e( 'Admin Controls', 'admincleanupcontrols' );
		echo '</h2>';
		echo '</div>';
		echo '<form method="POST">';
		echo '<label for="show_hide_admin"><strong>';
		esc_html_e( 'Minimal or advanced admin display', 'admincleanupcontrols' );
		echo ': </strong></label>';
		echo '<br />';
		echo '<select name="show_hide_admin" id="show_hide_admin">';
		echo '<option value="minimal">';
		esc_html_e( 'Minimal', 'admincleanupcontrols' );
		echo '</option>';
		echo '<option value="all"' . esc_attr( $selected ) . '>';
		esc_html_e( 'Advanced', 'admincleanupcontrols' );
		echo '</option>';
		echo '</select>';
		echo '<br /><br />';
		wp_nonce_field( 'mc-show-hide-admin-nonce' );
		echo '<input type="submit" value="Save" class="button button-primary button-large">';
		echo '</form>';
	}

}

$mc_admincleanupcontrols = new MC_AdminCleanupControls();
